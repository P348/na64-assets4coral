<Map>

<!-- MicroMega stations -->

   <!--
     Each MicroMega station has two coordinate planes X and Y.
     Each coordinate plane has 320 strips which multiplexed into 64 output channels
     ("detector" channels or "wires").
     Each APV chip has 128 input channels ("electronics" channels).
     All channels of a single MicroMega station (2 planes, 2 * 64 = 128 detector channels)
     are connected to a single APV chip with interleaving:
     
       APV channel 0 is plane X channel 0
       APV channel 1 is plane Y channel 0
                   2          X         1
                   3          Y         1
       ...
   -->
  
  <!-- test runs before session 2016A starts -->
  <ChipAPV
   options      = "rich_adc"
   runs         = "630-937"
   year         = "2016"
   version      = "1"
   detector     = "MicroMega, APV+ADC"
   maintainer   = "Emilio Depero">
   
   <!-- TBName    srcID  adcID chipID chanF chanS chanN   wireF wireL wireS wireP -->
    
     0  MM01X    622      0     15     0     2   64       0   63     1     0
     0  MM01Y    622      0     15     1     2   64       0   63     1     0
  </ChipAPV>
  
  <!-- mapping during 2016A session -->
  <ChipAPV
   options      = "rich_adc"
   runs         = "938-1059"
   year         = "2016"
   version      = "1"
   detector     = "MicroMega, APV+ADC"
   maintainer   = "Emilio Depero">
   
   <!-- TBName    srcID  adcID chipID chanF chanS chanN   wireF wireL wireS wireP -->
     0  MM01X    622      2     11     0     2   64       0   63     1     0
     0  MM01Y    622      2     11     1     2   64       0   63     1     0
     0  MM02X    622      2     15     0     2   64       0   63     1     0
     0  MM02Y    622      2     15     1     2   64       0   63     1     0
  </ChipAPV>

  <ChipAPV
   options      = "rich_adc"
   runs         = "1060-1297"
   year         = "2016"
   version      = "1"
   detector     = "MicroMega, APV+ADC"
   maintainer   = "Emilio Depero">
   
   <!-- TBName    srcID  adcID chipID chanF chanS chanN   wireF wireL wireS wireP -->
     0  MM03X    622      1      3     0     2   64       0   63     1     0
     0  MM03Y    622      1      3     1     2   64       0   63     1     0
     0  MM04X    622      1     15     0     2   64       0   63     1     0
     0  MM04Y    622      1     15     1     2   64       0   63     1     0
   
     0  MM01X    622      2     11     0     2   64       0   63     1     0
     0  MM01Y    622      2     11     1     2   64       0   63     1     0
     0  MM02X    622      2     15     0     2   64       0   63     1     0
     0  MM02Y    622      2     15     1     2   64       0   63     1     0
  </ChipAPV>
<!-- MM04 disconnected from GEM ADC-->
   <ChipAPV
   options      = "rich_adc"
   runs         = "1298-1371"
   year         = "2016"
   version      = "1"
   detector     = "MicroMega, APV+ADC"
   maintainer   = "Emilio Depero">
   
   <!-- TBName    srcID  adcID chipID chanF chanS chanN   wireF wireL wireS wireP -->
     0  MM03X    622      1      3     0     2   64       0   63     1     0
     0  MM03Y    622      1      3     1     2   64       0   63     1     0   
     0  MM01X    622      2     11     0     2   64       0   63     1     0
     0  MM01Y    622      2     11     1     2   64       0   63     1     0
     0  MM02X    622      2     15     0     2   64       0   63     1     0
     0  MM02Y    622      2     15     1     2   64       0   63     1     0
  </ChipAPV>
<!-- MM04 reconnected for magnet calibration-->
  <ChipAPV
   options      = "rich_adc"
   runs         = "1372-1390"
   year         = "2016"
   version      = "1"
   detector     = "MicroMega, APV+ADC"
   maintainer   = "Emilio Depero">
   
   <!-- TBName    srcID  adcID chipID chanF chanS chanN   wireF wireL wireS wireP -->
     0  MM03X    622      1      3     0     2   64       0   63     1     0
     0  MM03Y    622      1      3     1     2   64       0   63     1     0   
     0  MM04X    622      1     15     0     2   64       0   63     1     0
     0  MM04Y    622      1     15     1     2   64       0   63     1     0
     0  MM01X    622      2     11     0     2   64       0   63     1     0
     0  MM01Y    622      2     11     1     2   64       0   63     1     0
     0  MM02X    622      2     15     0     2   64       0   63     1     0
     0  MM02Y    622      2     15     1     2   64       0   63     1     0
  </ChipAPV>

  <!-- MM04 disconnected from GEM ADC-->
   <ChipAPV
   options      = "rich_adc"
   runs         = "1391-1426"
   year         = "2016"
   version      = "1"
   detector     = "MicroMega, APV+ADC"
   maintainer   = "Emilio Depero">
   
   <!-- TBName    srcID  adcID chipID chanF chanS chanN   wireF wireL wireS wireP -->
     0  MM03X    622      1      3     0     2   64       0   63     1     0
     0  MM03Y    622      1      3     1     2   64       0   63     1     0   
     0  MM01X    622      2     11     0     2   64       0   63     1     0
     0  MM01Y    622      2     11     1     2   64       0   63     1     0
     0  MM02X    622      2     15     0     2   64       0   63     1     0
     0  MM02Y    622      2     15     1     2   64       0   63     1     0
  </ChipAPV>

  <!-- MM04 connected to GEM ADC-->
   <ChipAPV
   options      = "rich_adc"
   runs         = "1427-1459"
   year         = "2016"
   version      = "1"
   detector     = "MicroMega, APV+ADC"
   maintainer   = "Emilio Depero">

   <!-- TBName    srcID  adcID chipID chanF chanS chanN   wireF wireL wireS wireP -->
     0  MM03X    622      1      3     0     2   64       0   63     1     0
     0  MM03Y    622      1      3     1     2   64       0   63     1     0
     0  MM04X    622      1     15     0     2   64       0   63     1     0
     0  MM04Y    622      1     15     1     2   64       0   63     1     0
     0  MM01X    622      2     11     0     2   64       0   63     1     0
     0  MM01Y    622      2     11     1     2   64       0   63     1     0
     0  MM02X    622      2     15     0     2   64       0   63     1     0
     0  MM02Y    622      2     15     1     2   64       0   63     1     0
  </ChipAPV>

<!-- MM04 disconnected from GEM ADC-->
   <ChipAPV
   options      = "rich_adc"
   runs         = "1460-1819"
   year         = "2016"
   version      = "1"
   detector     = "MicroMega, APV+ADC"
   maintainer   = "Emilio Depero">

   <!-- TBName    srcID  adcID chipID chanF chanS chanN   wireF wireL wireS wireP -->
    0  MM03X    622      1      3     0     2   64       0   63     1     0
    0  MM03Y    622      1      3     1     2   64       0   63     1     0
<!--
    0  MM04X    622      1     15     0     2   64       0   63     1     0
    0  MM04Y    622      1     15     1     2   64       0   63     1     0
-->
    0  MM01X    622      2     11     0     2   64       0   63     1     0
    0  MM01Y    622      2     11     1     2   64       0   63     1     0
    0  MM02X    622      2     15     0     2   64       0   63     1     0
    0  MM02Y    622      2     15     1     2   64       0   63     1     0
  </ChipAPV>

<!--October run-->
   <ChipAPV
   options      = "rich_adc"
   runs         = "1820-2558"
   year         = "2016"
   version      = "1"
   detector     = "MicroMega, APV+ADC"
   maintainer   = "Emilio Depero">

   <!-- TBName    srcID  adcID chipID chanF chanS chanN   wireF wireL wireS wireP -->
    0  MM03X    622      3      3     0     2   64       0   63     1     0
    0  MM03Y    622      3      3     1     2   64       0   63     1     0
    0  MM04X    622      3     15     0     2   64       0   63     1     0
    0  MM04Y    622      3     15     1     2   64       0   63     1     0
    0  MM01X    622      2     11     0     2   64       0   63     1     0
    0  MM01Y    622      2     11     1     2   64       0   63     1     0
    0  MM02X    622      2     15     0     2   64       0   63     1     0
    0  MM02Y    622      2     15     1     2   64       0   63     1     0
  </ChipAPV>

<!--LAB Cosmic run-->
   <ChipAPV
   options      = "rich_adc"
   runs         = "2559-2799"
   year         = "2016"
   version      = "1"
   detector     = "MicroMega, APV+ADC"
   maintainer   = "Emilio Depero">

   <!-- TBName    srcID  adcID chipID chanF chanS chanN   wireF wireL wireS wireP -->
    0  MM01X    622      0      3     0     2   64       0   63     1     0
    0  MM01Y    622      0      3     1     2   64       0   63     1     0
    0  MM02X    622      0      7     0     2   64       0   63     1     0
    0  MM02Y    622      0      7     1     2   64       0   63     1     0
    0  MM03X    622      0     11     0     2   64       0   63     1     0
    0  MM03Y    622      0     11     1     2   64       0   63     1     0
  </ChipAPV>

<!--September run 2017-->
   <ChipAPV
   options      = "rich_adc"
   runs         = "3001-9999999"
   year         = "2017"
   version      = "1"
   detector     = "MicroMega, APV+ADC"
   maintainer   = "Emilio Depero">

  <!-- TBName    srcID  adcID chipID chanF chanS chanN   wireF wireL wireS wireP -->
    0  MM08X    622      2     15     0     2   64       0   63     1     0
    0  MM08Y    622      2     15     1     2   64       0   63     1     0
    0  MM07X    622      2     11     0     2   64       0   63     1     0
    0  MM07Y    622      2     11     1     2   64       0   63     1     0
    0  MM06X    622      2      7     0     2   64       0   63     1     0
    0  MM06Y    622      2      7     1     2   64       0   63     1     0
    0  MM05X    622      2      3     0     2   64       0   63     1     0
    0  MM05Y    622      2      3     1     2   64       0   63     1     0
    0  MM03X    622      1     11     0     2   64       0   63     1     0
    0  MM03Y    622      1     11     1     2   64       0   63     1     0
    0  MM04X    622      1     15     0     2   64       0   63     1     0
    0  MM04Y    622      1     15     1     2   64       0   63     1     0
    0  MM01X    622      1      3     0     2   64       0   63     1     0
    0  MM01Y    622      1      3     1     2   64       0   63     1     0
    0  MM02X    622      1      7     0     2   64       0   63     1     0
    0  MM02Y    622      1      7     1     2   64       0   63     1     0
  </ChipAPV>
</Map>
