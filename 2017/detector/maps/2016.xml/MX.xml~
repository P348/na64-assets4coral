<Map>

<!-- MicroMega stations -->

   <!--
     Each MicroMega station has two coordinate planes X and Y.
     Each coordinate plane has 320 strips which multiplexed into 64 output channels
     ("detector" channels or "wires").
     Each APV chip has 128 input channels ("electronics" channels).
     All channels of a single MicroMega station (2 planes, 2 * 64 = 128 detector channels)
     are connected to a single APV chip with interleaving:
     
       APV channel 0 is plane X channel 0
       APV channel 1 is plane Y channel 0
                   2          X         1
                   3          Y         1
       ...
   -->
  
  <!-- test runs before session 2016A starts -->
  <ChipAPV
   options      = "rich_adc"
   runs         = "630-937"
   year         = "2016"
   version      = "1"
   detector     = "MicroMega, APV+ADC"
   maintainer   = "Dipanwita Banerjee">
   
   <!-- TBName    srcID  adcID chipID chanF chanS chanN   wireF wireL wireS wireP -->
    
     0  MX01X1__    622      0     15     0     2   64       0   63     1     0
     0  MX01Y1__    622      0     15     1     2   64       0   63     1     0
  </ChipAPV>
  
  <!-- mapping during 2016A session -->
  <ChipAPV
   options      = "rich_adc"
   runs         = "938-1059"
   year         = "2016"
   version      = "1"
   detector     = "MicroMega, APV+ADC"
   maintainer   = "Dipanwita Banerjee">
   
   <!-- TBName    srcID  adcID chipID chanF chanS chanN   wireF wireL wireS wireP -->
     0  MX01X1__    622      2     11     0     2   64       0   63     1     0
     0  MX01Y1__    622      2     11     1     2   64       0   63     1     0
     0  MX02X1__    622      2     15     0     2   64       0   63     1     0
     0  MX02Y1__    622      2     15     1     2   64       0   63     1     0
  </ChipAPV>

  <ChipAPV
   options      = "rich_adc"
   runs         = "1060-1297"
   year         = "2016"
   version      = "1"
   detector     = "MicroMega, APV+ADC"
   maintainer   = "Dipanwita Banerjee">
   
   <!-- TBName    srcID  adcID chipID chanF chanS chanN   wireF wireL wireS wireP -->
     0  MX03X1__    622      1      3     0     2   64       0   63     1     0
     0  MX03Y1__    622      1      3     1     2   64       0   63     1     0
     0  MX04X1__    622      1     15     0     2   64       0   63     1     0
     0  MX04Y1__    622      1     15     1     2   64       0   63     1     0
   
     0  MX01X1__    622      2     11     0     2   64       0   63     1     0
     0  MX01Y1__    622      2     11     1     2   64       0   63     1     0
     0  MX02X1__    622      2     15     0     2   64       0   63     1     0
     0  MX02Y1__    622      2     15     1     2   64       0   63     1     0
  </ChipAPV>
<!-- MX04 disconnected from GEM ADC-->
   <ChipAPV
   options      = "rich_adc"
   runs         = "1298-1371"
   year         = "2016"
   version      = "1"
   detector     = "MicroMega, APV+ADC"
   maintainer   = "Dipanwita Banerjee">
   
   <!-- TBName    srcID  adcID chipID chanF chanS chanN   wireF wireL wireS wireP -->
     0  MX03X1__    622      1      3     0     2   64       0   63     1     0
     0  MX03Y1__    622      1      3     1     2   64       0   63     1     0   
     0  MX01X1__    622      2     11     0     2   64       0   63     1     0
     0  MX01Y1__    622      2     11     1     2   64       0   63     1     0
     0  MX02X1__    622      2     15     0     2   64       0   63     1     0
     0  MX02Y1__    622      2     15     1     2   64       0   63     1     0
  </ChipAPV>
<!-- MX04 reconnected for magnet calibration-->
  <ChipAPV
   options      = "rich_adc"
   runs         = "1372-1390"
   year         = "2016"
   version      = "1"
   detector     = "MicroMega, APV+ADC"
   maintainer   = "Dipanwita Banerjee">
   
   <!-- TBName    srcID  adcID chipID chanF chanS chanN   wireF wireL wireS wireP -->
     0  MX03X1__    622      1      3     0     2   64       0   63     1     0
     0  MX03Y1__    622      1      3     1     2   64       0   63     1     0   
     0  MX04X1__    622      1     15     0     2   64       0   63     1     0
     0  MX04Y1__    622      1     15     1     2   64       0   63     1     0
     0  MX01X1__    622      2     11     0     2   64       0   63     1     0
     0  MX01Y1__    622      2     11     1     2   64       0   63     1     0
     0  MX02X1__    622      2     15     0     2   64       0   63     1     0
     0  MX02Y1__    622      2     15     1     2   64       0   63     1     0
  </ChipAPV>

  <!-- MX04 disconnected from GEM ADC-->
   <ChipAPV
   options      = "rich_adc"
   runs         = "1391-1426"
   year         = "2016"
   version      = "1"
   detector     = "MicroMega, APV+ADC"
   maintainer   = "Dipanwita Banerjee">
   
   <!-- TBName    srcID  adcID chipID chanF chanS chanN   wireF wireL wireS wireP -->
     0  MX03X1__    622      1      3     0     2   64       0   63     1     0
     0  MX03Y1__    622      1      3     1     2   64       0   63     1     0   
     0  MX01X1__    622      2     11     0     2   64       0   63     1     0
     0  MX01Y1__    622      2     11     1     2   64       0   63     1     0
     0  MX02X1__    622      2     15     0     2   64       0   63     1     0
     0  MX02Y1__    622      2     15     1     2   64       0   63     1     0
  </ChipAPV>

  <!-- MX04 connected to GEM ADC-->
   <ChipAPV
   options      = "rich_adc"
   runs         = "1427-1459"
   year         = "2016"
   version      = "1"
   detector     = "MicroMega, APV+ADC"
   maintainer   = "Dipanwita Banerjee">

   <!-- TBName    srcID  adcID chipID chanF chanS chanN   wireF wireL wireS wireP -->
     0  MX03X1__    622      1      3     0     2   64       0   63     1     0
     0  MX03Y1__    622      1      3     1     2   64       0   63     1     0
     0  MX04X1__    622      1     15     0     2   64       0   63     1     0
     0  MX04Y1__    622      1     15     1     2   64       0   63     1     0
     0  MX01X1__    622      2     11     0     2   64       0   63     1     0
     0  MX01Y1__    622      2     11     1     2   64       0   63     1     0
     0  MX02X1__    622      2     15     0     2   64       0   63     1     0
     0  MX02Y1__    622      2     15     1     2   64       0   63     1     0
  </ChipAPV>

<!-- MX04 disconnected from GEM ADC-->
   <ChipAPV
   options      = "rich_adc"
   runs         = "1460-1819"
   year         = "2016"
   version      = "1"
   detector     = "MicroMega, APV+ADC"
   maintainer   = "Dipanwita Banerjee">

   <!-- TBName    srcID  adcID chipID chanF chanS chanN   wireF wireL wireS wireP -->
    0  MX03X1__    622      1      3     0     2   64       0   63     1     0
    0  MX03Y1__    622      1      3     1     2   64       0   63     1     0
<!--
    0  MX04X    622      1     15     0     2   64       0   63     1     0
    0  MX04Y    622      1     15     1     2   64       0   63     1     0
-->
    0  MX01X1__    622      2     11     0     2   64       0   63     1     0
    0  MX01Y1__    622      2     11     1     2   64       0   63     1     0
    0  MX02X1__    622      2     15     0     2   64       0   63     1     0
    0  MX02Y1__    622      2     15     1     2   64       0   63     1     0
  </ChipAPV>

<!--October run-->
   <ChipAPV
   options      = "rich_adc"
   runs         = "1820-99999"
   year         = "2016"
   version      = "1"
   detector     = "MicroMega, APV+ADC"
   maintainer   = "Dipanwita Banerjee">

   <!-- TBName    srcID  adcID chipID chanF chanS chanN   wireF wireL wireS wireP -->
    0  MX03X1__    622      3      3     0     2   64       0   63     1     0
    0  MX03Y1__    622      3      3     1     2   64       0   63     1     0
    0  MX04X1__    622      3     15     0     2   64       0   63     1     0
    0  MX04Y1__    622      3     15     1     2   64       0   63     1     0
    0  MX01X1__    622      2     11     0     2   64       0   63     1     0
    0  MX01Y1__    622      2     11     1     2   64       0   63     1     0
    0  MX02X1__    622      2     15     0     2   64       0   63     1     0
    0  MX02Y1__    622      2     15     1     2   64       0   63     1     0
  </ChipAPV>


</Map>
