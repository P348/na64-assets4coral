# Assets for running COMPASS CORAL on NA64 data

This repo contains various assets providing shallow compatibility of the CORAL
framework with NA64 experiment: geometry, .opt files, etc.

The [CORAL](https://gitlab.cern.ch/compass/coral) is the framework for data
reconstruction developed by COMPASS (NA58) collaboration.

Initial content was initialized by Vladimir Kolosov's public dir content
(`~vkolosa/public/NA64_Setup_ALL/` on CERN AFS).

For usage reference see corresponding
[page on twiki](https://twiki.cern.ch/twiki/bin/view/P348/CoralReconstruction).

## Note on environment variable

In order to use `.opt` files, one have to set environment variables:
 - `NA64_FILES` to the path of this repository clone on some dir
 - `P348RECO_DIR` to the path of p348reco providing mapping files
See example of running CORAL at `exec/run-coral.sh`

