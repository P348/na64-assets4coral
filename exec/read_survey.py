# A script converting survey measurements of 2021-mu setup to CORAL's
# `detectors.dat'

import sys, re
import pandas as pd

# Regular expression to match with the line from survey table
rxTableLine = re.compile( r"^\s*(?P<name>[^\s]+)\s+(?P<face>[UDJSET])\s+"
        r"(?P<type>(\"[\w\s]+\")|([^\s]+))\s+(?P<sideJS>[JS])?\s+(?P<sideTB>[TB])?\s+"
        r"(?P<cX>~?-?[\d.]+)\s+(?P<cY>~?-?[\d.]+)\s+(?P<cZ>~?-?[\d.]+)\s+"
        r"(?P<oX>~?-?[\d.]+)\s+(?P<oY>~?-?[\d.]+)\s+(?P<oZ>~?-?[\d.]+)")

# Read the table
entries = []
with open('2021mu/assets/survey-2021-11-18.txt') as f:
    for nLine, l in enumerate(f):
        l = l.strip()
        if l.startswith('#'): continue  # comment
        if not l: continue  # empty line
        m = rxTableLine.match(l)
        if not m:
            sys.stderr.write(f'Warning: line {nLine} "{l}" does not match expected format.\n')
            continue
        entries.append(dict(m.groupdict()))

# Make the dataframe, force datatypes
df = pd.DataFrame(entries)
df = df.astype({ 'cX': float, 'cY': float, 'cZ': float
               , 'oX': float, 'oY': float, 'oZ': float
               })
df = df.set_index(['name', 'face', 'type'])
sdf = df.groupby(by=['name', 'face', 'type']).mean()  # , 'sideJS', 'sideTB'
pd.set_option('display.max_rows', None)  # XXX
print( sdf )

# For every GEM/MM, calculate positions using their average "Plane"
# downstream/upstream measurements
# ...
