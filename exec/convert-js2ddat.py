import sys, json, re, copy

with open(sys.argv[1]) as f:
    dd = json.load(f)

outStream = sys.stdout

# Info for detectors.dat, by detector kin
detAuxInfo = {
    'GM' : { 'nameFmt' : 'GM{stationNo:02d}{proj}1__'
        , 'id': 1
        , 'det': 'MG11'
        , 'unit': 1
        , 'type': 26
        , 'radLen': 1.43
        , 'Xsize': 10.6, 'Ysize': 10.6, 'Zsize': 0.05
        , 'rotMatr': 1
        , 'wireDist': -5.
        , 'angle': 0.
        , 'nWires' : 256
        , 'pitch' : 0.04
        , 'effic' : 1.
        , 'backgr': 0.0
        , 'tgate': 200.
    },
    'MM' : { 'nameFmt' : 'MX{stationNo:02d}{proj}1__'
        , 'id': 1
        , 'det': 'MG13'
        , 'unit': 1
        , 'type': 27
        , 'radLen': 1.5
        , 'Xsize': 8.1, 'Ysize': 8.1, 'Zsize': 0.05
        , 'rotMatr': 5
        , 'wireDist': -4.05
        , 'angle': 180.
        , 'nWires' : 320
        , 'pitch' : 0.025
        , 'effic' : 1.
        , 'backgr': 0.0
        , 'tgate': 200.
    }
}

planeAuxInfo = {
    'GM01Y1__': { 'id': 129, 'det': 'MG12', 'unit': 1, 'type': 26 },
    'GM01X1__': { 'id': 119, 'det': 'MG11', 'unit': 1, 'type': 26 },

    'GM02Y1__': { 'id': 130, 'det': 'MG12', 'unit': 2, 'type': 26 },
    'GM02X1__': { 'id': 120, 'det': 'MG11', 'unit': 2, 'type': 26 },

    'GM03Y1__': { 'id': 131, 'det': 'MG12', 'unit': 1, 'type': 26 },
    'GM03X1__': { 'id': 121, 'det': 'MG11', 'unit': 1, 'type': 26 },

    'GM04Y1__': { 'id': 132, 'det': 'MG12', 'unit': 2, 'type': 26 },
    'GM04X1__': { 'id': 122, 'det': 'MG11', 'unit': 2, 'type': 26 },

    'MX01U1__': { 'id': 101, 'det': 'MG13', 'unit': 1, 'type': 27 },
    'MX01V1__': { 'id': 111, 'det': 'MG14', 'unit': 1, 'type': 27 },

    'MX02U1__': { 'id': 103, 'det': 'MG13', 'unit': 2, 'type': 27 },
    'MX02V1__': { 'id': 113, 'det': 'MG14', 'unit': 2, 'type': 27 },

    'MX03U1__': { 'id': 103, 'det': 'MG13', 'unit': 3, 'type': 27 },
    'MX03V1__': { 'id': 113, 'det': 'MG14', 'unit': 3, 'type': 27 },

    'MX04U1__': { 'id': 104, 'det': 'MG13', 'unit': 4, 'type': 27 },
    'MX04V1__': { 'id': 114, 'det': 'MG14', 'unit': 4, 'type': 27 },

    'MX05U1__': { 'id': 105, 'det': 'MG13', 'unit': 5, 'type': 27 },
    'MX05V1__': { 'id': 115, 'det': 'MG14', 'unit': 5, 'type': 27 },

    'MX06U1__': { 'id': 106, 'det': 'MG13', 'unit': 6, 'type': 27 },
    'MX06V1__': { 'id': 116, 'det': 'MG14', 'unit': 6, 'type': 27 },

    'MX07U1__': { 'id': 107, 'det': 'MG13', 'unit': 7, 'type': 27 },
    'MX07V1__': { 'id': 117, 'det': 'MG14', 'unit': 7, 'type': 27 },
}

doSortByZ = False

def position_entries( dd_ ):
    rxJSName = re.compile(r'^(?P<jsDetName>[^\d]+)(?P<stationNo>\d+)_pos$')
    wm = ( (k, rxJSName.match(k), v) for k, v in dd_.items() )
    fd = ( e for e in wm if e[1] )
    for e in sorted(fd, key=lambda x: x[2][2]):
        if not e[1]: continue
        yield e

# Produces comment block referencing the original data from JSON and lines
# for `detectors.dat` with proper column order and units
for k, m, v in position_entries(dd):
    entryInfo = m.groupdict()
    #
    jsDetName = entryInfo['jsDetName']
    stationNo = int(entryInfo['stationNo'])
    #
    outStream.write('// %5s %12.4f %12.4f %12.4f\n'%(k, v[0], v[1], v[2]))
    if jsDetName.startswith('MM'):
        # MuMegas: consists of U,V planes, V is behind of U by 50 um
        stationName = 'MM%02d'%stationNo
        statProps = detAuxInfo['MM']
        planeProps = { 'U' : { 'angle': 180. }
                     , 'V' : { 'angle': -90., 'offset': {'Zcen': .005} }
                     }
    elif jsDetName.startswith('GEM'):
        # for GEMS: consists of X,Y planes, X is behind Y by 50um
        stationName = 'GM%02d'%stationNo
        statProps = detAuxInfo['GM']
        planeProps = { 'X' : { 'angle':  0. }
                     , 'Y' : { 'angle': 90., 'offset': {'Zcen': .005} }
                     }
    else:
        sys.stderr.write('Warning: position entry "%s" ignored.'%(jsDetName))
        continue
    for projLetter, planeProps in planeProps.items():
        props = copy.deepcopy(statProps)
        # override with per-projection properties
        props.update(planeProps)
        # add name
        detName = props['nameFmt'].format( stationName=stationName
                              , stationNo=stationNo
                              , proj=projLetter
                              )
        props['detName'] = detName
        # override with per-plane props
        props.update({
                'Xcen' : v[0]/10,
                'Ycen' : v[1]/10,
                'Zcen' : v[2]/10
            })
        #
        if detName in planeAuxInfo:
            props.update(planeAuxInfo[detName])
        # TODO: ...
        # Apply offsets, if need
        if 'offset' in props:
            for k in ('Xcen', 'Ycen', 'Zcen'):
                if k not in props['offset'].keys(): continue
                props[k] += props['offset'][k]
        line  = ' det {id:3d}  {detName:10s} {det:4s} {unit:2d} {type:3d} {radLen:5.2f}'.format(**props)
        line += ' {Zsize:10.4f} {Xsize:10.4f} {Ysize:10.4f}'.format(**props)
        line += ' {Zcen:12.4f} {Xcen:12.4f} {Ycen:12.4f}'.format(**props)
        line += ' {rotMatr:4d} {wireDist:12.4f} {angle:12.4f} {nWires:4d}'.format(**props)
        line += ' {pitch:14.7f} {effic:5.3f} {backgr:4.1f} {tgate:5.1f}'.format(**props)
        outStream.write(line + '\n')  # XXX

